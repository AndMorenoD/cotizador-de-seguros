import React, { Component } from 'react';
import Header from './Header';
import Formulario from './Formulario';
import {obtenerDiferenciaAnio,calcularMarca, obtenerPlan} from '../helper';


class App extends Component {

  cotizarSeguro = (datos) => {
    const {marca,plan,año} = datos;

    //agregar una base 
    let resultado = 2000;

    //obtener la diferencia de años y por cada año restar el 3%
    const diferencia = obtenerDiferenciaAnio(año);
    resultado -= ((diferencia*3) * resultado)/100
    
    //Americo 15% Asiatico 5% y europeo 30% de incremento al valor actual

    resultado = calcularMarca(marca) *  resultado;
    
    //plan basico 20% plan completo 50%

    let incrementoPlan = obtenerPlan(plan);

    resultado = parseFloat(incrementoPlan * resultado).toFixed(2);

    console.log(resultado);


  }
  render() {
    return (
      <div className="contenedor">
        <Header
         titulo = 'Cotizador de Seguros'
        />

        <div className="contenedor-formulario">
          <Formulario 
            cotizarSeguro = {this.cotizarSeguro}
          />
        </div>

      </div>
    );
  }
}

export default App;
